import { shallowMount } from '@vue/test-utils'
import MyRadioGroup from '@/components/MyRadioGroup.vue'

describe('MyRadioGroup.vue', () => {
  it('renders props when passed', () => {
    const name = 'test'
    const wrapper = shallowMount(MyRadioGroup, {
      propsData: { name }
    })
    expect(wrapper.props().name).toMatch(name)
  })
})
