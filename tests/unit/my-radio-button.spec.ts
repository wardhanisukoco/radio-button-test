import { shallowMount } from '@vue/test-utils'
import MyRadioButton from '@/components/MyRadioButton.vue'

describe('MyRadioButton.vue', () => {
  it('renders props when passed', () => {
    const label = 'Radio Test'
    const name = 'test'
    const wrapper = shallowMount(MyRadioButton, {
      propsData: { label },
      slots: {
        default: [
          '<input type="number" class="test-slot"/>',
          '<div class="test-slot">This is Slot.</div>'
        ]
      }
    })
    expect(wrapper.props().label).toMatch(label)
    expect(wrapper.props().name).toMatch(name)
    expect(wrapper.contains('input')).toBe(true)
  })
})
